%dw 2.4
output application/json
---
{
    "delta": false,
    "country": "GB",
    "records":  payload.request.data.dcsextractdata.dataheaders..dataheader map {
        "location": "945",
        "quantity" : if($.quantity_sign == "+")
        	$.soft_alloc_avail + $.total_alloc
          	else $.soft_alloc_avail + $.total_alloc + $.total_suspense,
        "sku":$.sku_id,
        "date": now()
    
    }
}