%dw 2.4 
fun mapReservedQty(data) = data.orders map ((item, index) -> (
					if ( item.orderType == "TRANSFER" ) item.qtyOrdered  else {}
				)) filter ((item, index) -> item !={} )
fun mapStockPositions(data: Object) =
	"UC_INV_RECONCILIATION_IFD": {
		"CTRL_SEG": {
			"TRNNAM": "INVENTORY RECONCILIATION",
			"TRNVER": "0002",
			"UC_INV_RECONCILIATION_IFD_SEG": data.request.data.dcsextractdata.dataheaders.*dataheader map (() -> {
				"LOCATION": "945",
				"ITEM_ID": $.sku_id,
				"TOTAL_QTY": if($.quantity_sign=='+') $.total_qty
                  else $.total_qty + $.total_suspense ,
				("TSF_RESERVED_QTY": mapReservedQty($))if(!isEmpty(mapReservedQty($))),
				"RTV_QTY": 0,
				"FINISHER_UNITS": 0,
				"AVAILABLE_QTY": if($.quantity_sign=='+')
                 $.soft_alloc_avail + $.total_alloc
                  else $.soft_alloc_avail + $.total_alloc + $.total_suspense,
				"PACK_IND": "N",
				"UNAVAILABLE_BUCKET": {
					"INVSTS": "TRBL",
					"QTY": $.total_locked + $.total_suspense
				}
			})
		}
	}

