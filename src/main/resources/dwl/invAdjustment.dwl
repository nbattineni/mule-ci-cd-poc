%dw 2.4
import callOracleRule from rules::checkCodeToProcessRule
output application/json

fun formatDate(data) = if(!isEmpty(data.time_zone_name))
 ((data.dstamp ++ ' '++ data.time_zone_name) as DateTime {format: "yyyyMMddHHmmss z"} >> 'UTC') as DateTime {format: "yyyy-MM-dd HH:MM:SS"}
  else 
   ((data.dstamp ++ ' Z') as DateTime {format: "yyyyMMddHHmmss z"} >> 'UTC') as DateTime {format: "yyyy-MM-dd HH:MM:SS"}
---
vars.data.request.data.dcsextractdata.dataheaders.*dataheader map{
        "item_id": $.sku_id,
        "adjustment_reason_code": callOracleRule($, true),
        "unit_qty": if($.update_quantity_sign == "-")
            $.update_quantity_sign ++ $.update_qty
            else $.update_qty,
        (callOracleRule($, false)),
        "user_id": "Clipper",
        "create_date": formatDate($)
    }