%dw 2.4
output application/json
---
{
	errorMessageVals: {
		clientId: "Trinity",
		clientName: "Trinity",
		objectReferenceId: "NA",
		transactionId: correlationId,
		fullErrorDetailsInMulesoft: 'Object store error, kindly check logs.'
	},
	errorMessageMetadata: {
		errorSubject: p("error.subject.internal"),
		sendAlertToOps: "INFO" ,
		sendAlertToClprServiceDesk: "CRITICAL",
		sendAlertToCustServiceDesk: "N/A"
	}
}