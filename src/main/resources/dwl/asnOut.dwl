%dw 2.4
import module::commonAsnOut as commonAsnOut
output application/xml
fun formatDate(data) = if ( !isEmpty(data.time_zone_name) ) 
(data.shipped_date ++ ' ' ++ data.time_zone_name) as DateTime {
	format: "yyyyMMddHHmmss z"
} >> 'UTC'
  else 
  (data.shipped_date ++ ' Z') as DateTime {
	format: "yyyyMMddHHmmss z"
} >> 'UTC'

---
{
	"ASN": {
		((payload.request.data.dcsextractdata.dataheaders.*dataheader filter ($.order_type == "CC") ++ (payload.request.data.dcsextractdata.dataheaders.*dataheader filter ($.order_type != "CC")
			))
    	
    	map (data) -> ("ASNOutDesc": {
			"to_location": if (upper(data.customer_id) startsWith "PM") (upper(data.customer_id) replace 'PM' with "") else data.customer_id,
			"to_loc_type": data.user_def_type_3,
			"from_location": "945",
			"from_loc_type": "W",
			"bol_nbr": data.order_id,
			"shipment_date": formatDate(data),
			"ASNOutDistro": {
				"distro_nbr": if ( data.order_type == "CC" ) commonAsnOut::order_id_wrapped(vars.distroNbr.unique_order_id + $$) else data.order_id,
				"distro_doc_type": "T",
				"cust_order_nbr": if ( data.order_type == "CC" ) data.order_id else "",
				"ASNOutCtn": {
					"final_location": if ( data.order_type == "CC" ) "516" else "",
					"container_id": "0",
					(data.datalines.*dataline map ("ASNOutItem": {
						"item_id": $.sku_id,
						"unit_qty": $.qty_shipped,
						"from_disposition": "ATS"
					}))
				}
			}
		}))
	}
}