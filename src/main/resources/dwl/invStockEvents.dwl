%dw 2.4
import callOmsRule from rules::checkCodeToProcessRule
output application/json 

fun formatDate(data) = if(!isEmpty(data.time_zone_name))
 (data.dstamp ++ ' '++ data.time_zone_name) as DateTime {format: "yyyyMMddHHmmss z"} >> 'UTC'
  else 
      (data.dstamp ++ ' Z') as DateTime {format: "yyyyMMddHHmmss z"} >> 'UTC'
 ---
vars.data.request.data.dcsextractdata.dataheaders.*dataheader map{
        "location": "945",
        "quantity" : callOmsRule($),
        "sku":$.sku_id,
        "date": formatDate($)
    }
 

