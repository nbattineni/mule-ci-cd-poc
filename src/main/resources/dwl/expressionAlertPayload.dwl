%dw 2.4
output application/json
---
{
	errorMessageVals: {
		clientId: "Trinity",
		clientName: "Trinity",
		objectReferenceId: "NA",
		transactionId: correlationId,
		fullErrorDetailsInMulesoft: 'DWL transformation error, kindly check logs.'
	},
	errorMessageMetadata: {
		errorSubject: p("error.subject.expression"),
		sendAlertToOps: "INFO" ,
		sendAlertToClprServiceDesk: "CRITICAL",
		sendAlertToCustServiceDesk: "N/A"
	}
}